/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmobiliariatcy;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author alcokond
 */
public class Casa implements Serializable, Comparable<Casa> {
    private Urbanizacion urb;
    private Etapa et;
    private String nombreModelo;
    private int plantas;
    private int cuartos;
    private int  cocina;
    private int cuartoServicio;
    private int baños;
    private int metrosConstruccion;
    private String rutaImagen;
    private double precio;

    public Casa(String nombreModelo, int plantas, int cuartos, int cocina, int cuartoServicio, int baños, int metrosConstruccion,double precio) {
        this.urb = null;
        this.et = null;
        this.nombreModelo = nombreModelo;
        this.plantas = plantas;
        this.cuartos = cuartos;
        this.cocina = cocina;
        this.cuartoServicio = cuartoServicio;
        this.baños = baños;
        this.metrosConstruccion = metrosConstruccion;
        this.rutaImagen = "";
        this.precio = precio;
    }

    public Casa() {
        this.urb = null;
        this.et = null;
        this.nombreModelo = "";
        this.plantas = 0;
        this.cuartos = 0;
        this.cocina = 0;
        this.cuartoServicio = 0;
        this.baños = 0;
        this.metrosConstruccion = 0;
        this.rutaImagen = "";
        this.precio = 0;
    }

    public String getNombreModelo() {
        return nombreModelo;
    }

    public void setNombreModelo(String nombreModelo) {
        this.nombreModelo = nombreModelo;
    }

    public int getPlantas() {
        return plantas;
    }

    public void setPlantas(int plantas) {
        this.plantas = plantas;
    }

    public int getCuartos() {
        return cuartos;
    }

    public void setCuartos(int cuartos) {
        this.cuartos = cuartos;
    }

    public int isCocina() {
        return cocina;
    }

    public void setCocina(int cocina) {
        this.cocina = cocina;
    }

    public int getCuartoServicio() {
        return cuartoServicio;
    }

    public void setCuartoServicio(int cuartoServicio) {
        this.cuartoServicio = cuartoServicio;
    }

    public int getBaños() {
        return baños;
    }

    public void setBaños(int baños) {
        this.baños = baños;
    }

    public int getMetrosConstruccion() {
        return metrosConstruccion;
    }

    public void setMetrosConstruccion(int metrosConstruccion) {
        this.metrosConstruccion = metrosConstruccion;
    }

    public String getRutaImagen() {
        return rutaImagen;
    }

    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Urbanizacion getUrb() {
        return urb;
    }

    public void setUrb(Urbanizacion urb) {
        this.urb = urb;
    }

    public Etapa getEt() {
        return et;
    }

    public void setEt(Etapa et) {
        this.et = et;
    }

    
    @Override
    public String toString() {
        return "Modelo: " + nombreModelo + "\n" + plantas+ " plantas " + cuartos + " cuartos " + cocina + " cocina " +  cuartoServicio + " cuartoServicio " + baños + " baños " ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.urb);
        hash = 83 * hash + Objects.hashCode(this.et);
        hash = 83 * hash + Objects.hashCode(this.nombreModelo);
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.precio) ^ (Double.doubleToLongBits(this.precio) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Casa other = (Casa) obj;
        if (Double.doubleToLongBits(this.precio) != Double.doubleToLongBits(other.precio)) {
            return false;
        }
        if (!Objects.equals(this.nombreModelo, other.nombreModelo)) {
            return false;
        }
        if (!Objects.equals(this.urb, other.urb)) {
            return false;
        }
        if (!Objects.equals(this.et, other.et)) {
            return false;
        }
        return true;
    }

    

    @Override
    public int compareTo(Casa o) {
        if(this.precio>o.precio) return 1;
        if(this.precio<o.precio) return -1;
        else return 0;
    }

    
    
}
