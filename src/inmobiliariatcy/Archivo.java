/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmobiliariatcy;

/**
 * Interface para dar formato Archivo a las clases
 * @author Alan Coello
 * @param <E>
 */
public interface Archivo {
    String getFileFormat();

}
