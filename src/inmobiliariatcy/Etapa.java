/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmobiliariatcy;

import inmobiliariatcyrepo.Casa;
import inmobiliariatcyrepo.Urbanizacion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author alcokond
 */
public class Etapa implements Serializable {
    private Urbanizacion urbanizacion;
    private String nombre;
    private boolean enVenta;
    private ArrayList<Servicio> servicios = new ArrayList<>();
    private ArrayList<Casa> casas = new ArrayList<>();
    private int manzanas;
    private int lotes;

    public Etapa() {
    }

    public Etapa(String nombre, int manzanas, int lotes) {
        this.urbanizacion = null;
        this.nombre = nombre;
        this.manzanas = manzanas;
        this.lotes = lotes;
        this.enVenta = false;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEnVenta() {
        return enVenta;
    }

    public void setEnVenta(boolean enVenta) {
        this.enVenta = enVenta;
    }

    public ArrayList<Servicio> getServicios() {
        return servicios;
    }

    public void setServicios(ArrayList<Servicio> servicios) {
        this.servicios = servicios;
    }

    public int getManzanas() {
        return manzanas;
    }

    public void setManzanas(int manzanas) {
        this.manzanas = manzanas;
    }

    public int getLotes() {
        return lotes;
    }

    public void setLotes(int lotes) {
        this.lotes = lotes;
    }

    public Urbanizacion getUrbanizacion() {
        return urbanizacion;
    }

    public void setUrbanizacion(Urbanizacion urbanizacion) {
        this.urbanizacion = urbanizacion;
    }

    public ArrayList<Casa> getCasas() {
        return casas;
    }

    public void setCasas(ArrayList<Casa> casas) {
        this.casas = casas;
    }
    
    

    @Override
    public String toString() {
        return "Etapa{" + "urbanizacion=" + urbanizacion.getNombre() + ", nombre=" + nombre + ", enVenta=" + enVenta + ", servicios=" + servicios + ", manzanas=" + manzanas + ", lotes=" + lotes + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.urbanizacion);
        hash = 41 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Etapa other = (Etapa) obj;
        if (this.manzanas != other.manzanas) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.urbanizacion, other.urbanizacion)) {
            return false;
        }
        return true;
    }

    
    
    
}
