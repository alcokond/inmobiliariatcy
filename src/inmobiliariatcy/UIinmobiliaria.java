/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmobiliariatcy;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Clase donde estarán todos los menúes que se le presentará al usuario
 * @author alcokond
 */
public class UIinmobiliaria {
    /**
     * Objeto Scanner que se utiliza en todas las llamadas por teclado
     */
    public static Scanner teclado = new Scanner(System.in);
    /**
     * Listas volátiles para almacenar información (para conservar la información agregada por el usuario se serializa)
     */
    public static ArrayList<Urbanizacion> urbanizaciones = new ArrayList<>();
    public static ArrayList<Casa> casas = new ArrayList<>();
    public static ArrayList<Servicio> servicios = new ArrayList<>();
    public static String urFileName = "urbanizaciones.ser";
    public static ArrayList<String> rutas = new ArrayList<>();
    /**
     * método main donde se ejecutará todo el programa
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        llenarRutas();
        fillServices();
        // Si no existe el archivo, lo crea (de esta forma no me genera error si quiero leer o escribir en él)
        File file = new File(urFileName);
        try {
            file.createNewFile();
        } catch (IOException ex) {
            System.err.println("No se creó el archivo "+urFileName);
        }
        // lleno la lista urbanizaciones antes de ejecutar el programa
        urbanizaciones = deserializar(urFileName);

        int opcion;
        String validar = "N";
        /**
         * Comienzo del programa 
         */
        while ("N".equals(validar)){
            System.out.println("********  INMOBILIARIA TU CASA YA ********");
            System.out.println("1. Cotizar casas");
            System.out.println("2. Entrar al sistema inmobiliaria");
            System.out.println("3. Salir");
            System.out.println("Ingrese el número de la opción a la que desea ingresar: ");
            opcion = teclado.nextInt();
            
            validarRango(opcion, 3);
            /**
             * switch del menú principal
             */
            switch(opcion){
                case 2:
                    
                    /**
                     * switch del submenú de opción 2
                     */
                    String ingresar = "N";
                    
                    while ("N".equals(ingresar)) {
                        String usuario;
                        String contraseña;
                        int op1;
                        //para ingresar a la opción 2 se tiene que tener un usuario
                        System.out.println("||||||||||||  Datos de usuario  ||||||||||||||");
                        System.out.println("1. Ingresar");
                        System.out.println("2. Crear una cuenta");
                        System.out.println("Ingrese el número de la opción a la que desea ingresar: ");
                        op1 = teclado.nextInt();

                        validarRango(op1, 2);
                        switch (op1) {
                            case 1:
                                String validar1 = "S";
                                
                                while (validar1.equals("S")) {
                                    System.out.println("Ingrese su nombre de usuario: ");
                                    usuario = teclado.next();
                                    System.out.println("Ingrese su contraseña: ");
                                    contraseña = teclado.next();
                                    if (validarUsuario(usuario, contraseña)) {
                                        System.out.println("Usuario correcto");
                                        ingresar = "S";
                                        validar1 = "N";
                                    }else{
                                        System.out.println("Usuario incorrecto");
                                        System.out.println("¿Deseas volver a ingresar los datos?(S/N): ");
                                        validar1 = teclado.next().toUpperCase();
                                        
                                    }
                                }
                                

                                break;

                            case 2:
                                String v = "N";
                                while ("N".equals(v)) {
                                    System.out.println("Ingrese su nombre de usuario: ");
                                    usuario = teclado.next();
                                    System.out.println("Ingrese su contraseña: ");
                                    contraseña = teclado.next();
                                    System.out.println("¿Los datos son correctos?(S/N): ");
                                    v = teclado.next().toUpperCase();
                                    if ("S".equals(v)) {
                                        registrarUsuario(usuario, contraseña);
                                        ingresar = "N";
                                    }
                                }
                                break;

                            default:
                                break;

                        }
                    }
                    
                    // inicio de la opción 2
                    String salir = "N";
                    while("N".equals(salir)){
                        menu2();
                        System.out.println("¿Desea regresar al menú principal?(S/N): ");
                        salir = teclado.next().toUpperCase();
                    }
                    break;
                    
                case 1:
                    System.out.println("*******  SISTEMA DE COTIZACIÓN   ********");
                    System.out.println("¡Hola! Me llamo Luz y seré tu asistente para que puedas elegir la mejor opción.");
                    System.out.println("Primero comencemos por sus datos personales.");
                    String validate = "N";
                    String correo ="";
                    while ("N".equals(validate)) {
                        System.out.println("¿Cuál es tu nombre?");
                        String name = teclado.next();
                        System.out.println("Dinos tu correo: ");
                        String mail = teclado.next();
                        while(!validarCorreo(mail)) {
                            System.out.println(name+"el correo que ingresaste no es válido. Intenta de nuevo:");
                            mail = teclado.next();
                        }
                        correo = mail;
                        System.out.println("¿Los datos son correctos?(S/N): ");
                        validate = teclado.next().toUpperCase();
                    }
                     menu1(correo);
                     
                    break;
                case 3:
                    break;
                default:
                    break;
            }
            System.out.println("¿Desea salir del progama?(S/N): ");
            
            validar = teclado.next().toUpperCase();
        }
    }
    
    
    
    /**
     * Método para comprobar la estructura del correo
     * @param correo
     * @return 
     */
    public static boolean validarCorreo(String correo){
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(correo);
        
        return matcher.matches();
    }
    /**
     * Método que recibe un nuevo usuario y contraseña para ser escrita en un archivo
     * @param usuario
     * @param contraseña 
     */
    public static void registrarUsuario(String usuario, String contraseña){
        String info = usuario+","+contraseña;
        escribirArchivo("usuarios.txt", info);
        System.out.println("Usuario registrado con éxito");
    }
    /**
     * Método para revisar que el usuario se encuentre en el archivo "usuarios.txt"
     * @param usuario
     * @param contraseña
     * @return 
     */
    public static boolean validarUsuario(String usuario, String contraseña){
        
        try (Scanner sc = new Scanner(new File("usuarios.txt"))) {
            while (sc.hasNext()) {
                String[] list = sc.next().split(",");
                if (list[0].equals(usuario) && list[1].equals(contraseña)){
                    System.out.println("Usuario válido");
                    return true;
                }
  
            }
            System.out.println("Usuario no válido");
        } catch (FileNotFoundException e) {
            System.err.println("Archivo no encontrado");
        }
        return false;
    }
    /**
     * Método para escribir información en un archivo
     * @param filename
     * @param info 
     */
    public static void escribirArchivo(String filename, String info){
        
        try (BufferedWriter escritor = new BufferedWriter(new FileWriter(filename, true))){
            escritor.write(info);
            escritor.newLine();
        } catch (FileNotFoundException ex) {
            System.err.println("Archivo no encontrado");
        } catch (IOException ex) {
            System.err.println("Error al leer el archivo");
        }
    }
    /**
     * Método encargado de realizar todas las opciones del menú 2
     */
    public static void menu2(){
        
        System.out.println("***** BIENVENIDO *****");
        System.out.println("Esta sección permite manejar el registro, consulta y eliminación \n de Urbanizaciones, Etapas, Lotes y Casas");
        System.out.println("1. Crear");
        System.out.println("2. Consultar");
        System.out.println("3. Eliminar");
        System.out.println("4. Ir al menú principal");
        System.out.println("Escoja el número de la opción a la que desee acceder: ");
        int opcion = teclado.nextInt();
        validarRango(opcion, 4);
        if(opcion!=4){
            submenu2(opcion);
        }
        ArrayList<Urbanizacion> urbs = urbanizaciones; //No se pueden serializar objetos estáticos, por eso lo almaceno en una nueva variable
        serializar(urFileName, urbs);
        
    }
    /**
     * Método auxiliar que contiene un submenú de la opción 2
     * En este se hace el registro, consulta y eliminación de casas, urbanizaciones, lotes y etapas
     * @param opcion 
     */
    public static void submenu2(int opcion){
        System.out.println("**** OPCIONES *****");
        System.out.println("1. Urbanización");
        System.out.println("2. Etapa");
        System.out.println("3. Lote");
        System.out.println("4. Casa");
        System.out.println("5. Regresar");
        System.out.println("Ingrese el número de la opción: ");
        int submenu2 = teclado.nextInt();
        validarRango(submenu2, 4);
        switch (submenu2) {
            case 1:
                switch (opcion) {
                    case 1:
                        Urbanizacion u = new Urbanizacion();
                        String valida = "N";
                        while ("N".equals(valida)) {
                            System.out.println("**** REGISTRO DE URBANIZACIONES ****");
                            System.out.println("Ingrese el nombre de la urbanización: ");
                            String nombre = teclado.next();
                            System.out.println("Ingrese la direccion: ");
                            String dir = teclado.next();
                            System.out.println("¿Los datos están correctos?(S/N)");
                            valida = teclado.next().toUpperCase();
                            if ("S".equals(valida)){
                                u.setNombre(nombre);
                                u.setDireccion(dir);
                            }
                            
                        }
                        registrar(u);
                        break;
                    case 2:
                        System.out.println("**** CONSULTA DE URBANIZACIONES ****");
                        Urbanizacion ub = getUrbanizacion();
                        System.out.println(ub);
                        System.out.println(printEtapas(ub));
                        break;
                    case 3:
                        System.out.println("**** ELIMINACION DE URBANIZACIONES ****");
                        urbanizaciones.remove(getUrbanizacion());
                        System.out.println("La etapa se eliminó exitosamente");
                        break;
                    default:
                        break;
                }
                
                break;              
                
            case 2:
                switch (opcion) {
                    case 1:
                        Etapa e = new Etapa();
                        String valida = "N";
                        while ("N".equals(valida)) {
                            System.out.println("**** REGISTRO DE ETAPAS ****");
                            System.out.println("Ingrese el nombre de la etapa: ");
                            String nombre = teclado.next();
                            System.out.println("Ingrese el número de manzanas: ");
                            int mazanas = teclado.nextInt();
                            System.out.println("Ingrese el número de lotes: ");
                            int lots = teclado.nextInt();
                            e.setNombre(nombre);
                            e.setManzanas(mazanas);
                            e.setLotes(lots);
                            System.out.println("¿Los datos están correctos?(S/N)");
                            valida = teclado.next().toUpperCase();
                        }
                        registrar(e);
                        break;
                    case 2:
                        System.out.println("**** CONSULTA DE ETAPAS ****");
                        System.out.println("Primero debe escoger a qué urbanización pertenece");
                        System.out.println(getEtapa(getUrbanizacion()));
                        break;
                    case 3:
                        System.out.println("**** ELIMINACION DE ETAPAS ****");
                        Urbanizacion urr = getUrbanizacion();
                        Etapa ett = getEtapa(getUrbanizacion());
                        urr.getEtapas().remove(ett);
                        System.out.println("La etapa se eliminó exitosamente");
                        break;
                    default:
                        break;
                }
                break;
            case 3:
                switch (opcion) {
                    case 1:
                        Lote lott = new Lote();
                        String valida = "N";
                        while ("N".equals(valida)) {
                            System.out.println("**** REGISTRO DE LOTES ****");
                            System.out.println("Ingrese el nombre o número de manzana: ");
                            String manzana = teclado.next();
                            System.out.println("Ingrese el tamaño en metros cuadrados: ");
                            int tamano = teclado.nextInt();
                            lott.setManzana(manzana);
                            lott.setTamaño(tamano);
                            System.out.println("¿Los datos están correctos?(S/N)");
                            valida = teclado.next().toUpperCase();
                        }
                        registrar(lott);
                        break;
                    case 2:
                        System.out.println("**** CONSULTA DE LOTES ****");
                        System.out.println("Primero debe escoger a qué urbanización pertenece");
                        System.out.println(getLote(getUrbanizacion()));
                        break;
                    case 3:
                        System.out.println("**** ELIMINACION DE LOTES ****");
                        Urbanizacion urr = getUrbanizacion();
                        Lote lote = getLote(urr);
                        urr.getLotes().remove(lote);
                        urbanizaciones.remove(urr);
                        urbanizaciones.add(urr);
                        System.out.println("El lote se eliminó exitosamente");
                        break;
                    default:
                        break;
                }
                break;
            case 4:
                switch (opcion) {
                    case 1:
                        Casa casa = null;
                        String valida = "N";
                        while ("N".equals(valida)) {
                            System.out.println("**** REGISTRO DE CASAS/DEPARTAMENTOS ****");
                            System.out.println("¿Desea registrar una casa o un departamento?(C/D): ");
                            String choice = teclado.next().toUpperCase();
                            System.out.println("Ingrese el nombre del modelo: ");
                            String nombre = teclado.next();
                            System.out.println("Ingrese el número de plantas: ");
                            int plantas = teclado.nextInt();
                            System.out.println("Ingrese el número de cuartos: ");
                            int cuartos = teclado.nextInt();
                            System.out.println("Ingrese el número de cocinas: ");
                            int cocina = teclado.nextInt();
                            System.out.println("Ingrese el número de cuartos de servicio: ");
                            int servicio = teclado.nextInt();
                            System.out.println("Ingrese el número de baños: ");
                            int banos = teclado.nextInt();
                            System.out.println("Ingrese el número de metros cuadrados por construcción: ");
                            int metros = teclado.nextInt();
                            System.out.println("Ingrese el precio: ");
                            double precio = teclado.nextDouble();
                            if(choice.equals("D")){
                                System.out.println("Ingrese el número de lavanderías: ");
                                int lav = teclado.nextInt();
                                System.out.println("Ingrese el número de parqueaderos: ");
                                int par = teclado.nextInt();
                                System.out.println("Ingrese el número de ascensores: ");
                                int as = teclado.nextInt();
                                casa = new Departamento(lav,par,as, nombre, plantas, cuartos, cocina, servicio, banos, metros, precio);
                                casa.setRutaImagen(getRandomRute());
                            }else{
                                casa = new Casa(nombre, plantas, cuartos, cocina, servicio, banos, metros, precio);
                                casa.setRutaImagen(getRandomRute());
                            }
                            System.out.println("¿Los datos están correctos?(S/N)");
                            valida = teclado.next().toUpperCase();
                        }
                        registrar(casa);
                        break;
                    case 2:
                        System.out.println("**** CONSULTA DE CASAS/DEPARTAMENTOS ****");
                        System.out.println("Primero debe escoger a qué urbanización pertenece");
                        Urbanizacion u = getUrbanizacion();
                        Etapa e = getEtapa(u);
                        System.out.println(getCasa(e));
                        break;
                    case 3:
                        System.out.println("**** ELIMINACION DE CASAS/DEPARTAMENTOS ****");
                        Urbanizacion urr = getUrbanizacion();
                        Etapa ett = getEtapa(urr);
                        Casa cas = getCasa(ett);
                        ett.getCasas().remove(cas);
                        urr.setEtapas(urr.getEtapas());
                        urbanizaciones.remove(urr);
                        urbanizaciones.add(urr);
                        System.out.println("La casa se eliminó exitosamente");
                        break;
                    default:
                        break;
                }
                break;
            case 5:
                menu2();
                break;
            default:
                break;
        }
        
    }
    /**
     * Mñetodo encargado de mostrar las opciones del menú 1
     * @param correo
     */
    public static void menu1(String correo){
        System.out.println("ºººººº  Criterios de cotización  ºººººº");
        System.out.println("¿Buscas casa o departamento?");
        String tipo = teclado.next().toLowerCase();
        System.out.println("¿De cuántas plantas? Puedes escribir “omitir” en caso de no tener preferencia: ");
        String plantas = teclado.next();
        System.out.println("¿Número mínimo de cuartos? Puedes escribir “omitir” en caso de no tener preferencia: ");
        String cuartos = teclado.next();
        System.out.println("¿Número mínimo de baños? Puedes escribir “omitir” en caso de no tener preferencia: ");
        String baños = teclado.next();
        System.out.println("¿En qué sector? Puedes escribir “omitir” en caso de no tener preferencia: ");
        String sector = teclado.next();
        System.out.println("¿Cuál es su sueldo mensual?(Conjunto si es casado(a))");
        double sueldo = teclado.nextDouble();
        System.out.println("¿Cuál es su capital de entrada?");
        double entrada = teclado.nextDouble();
        
        boolean contiene = false;
        ArrayList<Casa> casasParaCorreo = new ArrayList<>();
        ArrayList<Casa> affordable = casasEncontradas(tipo, calcularPresupuesto(entrada, sueldo));
        ArrayList<Casa> chosen = escogerCasas(affordable, plantas, cuartos, baños, sector);
        boolean cho = false;
        if(chosen.isEmpty() && !affordable.isEmpty()){
            System.out.println(" |||||||||||||| No hemos encontrado casas de acuerdo a tus requerimientos, pero estas podrían gustarte: ||||||||||||||");
            imprimir(affordable);
            casasParaCorreo = affordable;
            Collections.sort(casasParaCorreo);
            contiene = true;
        }else if(affordable.isEmpty() && chosen.isEmpty()){
            System.out.println("|||||||||||||| No se hallaron resultados ||||||||||||||");
        }else{
            cho = true;
            System.out.println("|||||||||||||| Hemos encontrado las siguientes coincidencias: ||||||||||||||");
            imprimir(chosen);
            casasParaCorreo = chosen;
            Collections.sort(casasParaCorreo);
            contiene = true;
        }
        
        if (contiene) {
            boolean bandera = true;
            int tamaño = 0;
            if (cho) {
                tamaño = chosen.size();
            } else {
                tamaño = affordable.size();
            }
            
            ArrayList<Integer> opciones_num = new ArrayList<>();
            while (bandera) {
                System.out.println("\nSelecciona los modelos que desees separados por coma y nosotros te enviaremos un correo con la información detallada: ");
                System.out.println("Recuerde que sólo puede ingresar opciones entre 1-" + tamaño);
                String opciones = teclado.next();
                if (opciones.contains(",")) {
                    String[] lista_opciones = opciones.split(",");
                    for (String opcion : lista_opciones) {
                        int op = Integer.parseInt(opcion);
                        opciones_num.add(op);
                        bandera = op < 1 || op > tamaño;
                    }
                }else{
                   int op = Integer.parseInt(opciones);
                   opciones_num.add(op);
                    bandera = op < 1 || op > tamaño;
                }
                
            }
            
            for(Integer i: opciones_num){
                Casa casa = getCasaCorreo(i, casasParaCorreo);
                
                String infoAdicional = String.valueOf("Metros de construccion: "+casa.getMetrosConstruccion()+"\n\nPrecio: "+casa.getPrecio());
                String mensaje = casa.getUrb().toString() +"\n\n" + casa.toString()+"\n\n"+ infoAdicional;
                
                enviarCorreo(correo, "Inmobiliaria TCY " , mensaje, casa.getRutaImagen());
            }
            System.out.println("Correo(s) enviados exitosamente");
            
        }
        
    }
    /**
     * Método para obtener la casa que se va a enviar al correo
     * @param opcion
     * @param casasParaCorreo
     * @return 
     */
    public static Casa getCasaCorreo(int opcion, ArrayList<Casa> casasParaCorreo ){
        return casasParaCorreo.get(opcion-1);
    }
    
    

    /**
     * Metodo que envia un correo electrónico
     * TODO: modificar el correo remitente
     * @param correo
     * @param asunto
     * @param mensaje
     * @param rutaImagen
     */
    public static void enviarCorreo(String correo, String asunto, String mensaje, String rutaImagen) {

        final String username = "proyectopoo9@gmail.com";
        final String password = "proyectoPoo2018";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(correo));
            message.setSubject(asunto);
            
            BodyPart texto = new MimeBodyPart();
            texto.setText(mensaje);
            
            BodyPart adjunto = new MimeBodyPart();
            adjunto.setDataHandler(new DataHandler(new FileDataSource(rutaImagen)));
            
            MimeMultipart multiParte = new MimeMultipart();

            multiParte.addBodyPart(texto);
            multiParte.addBodyPart(adjunto);
            message.setContent(multiParte);
            
            Transport.send(message);

        } catch (MessagingException e) {
            System.err.println("Error en el envío del correo");
        }
    }
    
    public static void imprimir(ArrayList<Casa> cc){
        int i = 1;
        for(Casa c: cc){
            System.out.println("Urbanización "+c.getUrb().getNombre());
            System.out.println(i+". "+c);
            i++;
        }
    }
    
    public static ArrayList<Casa> escogerCasas(ArrayList<Casa> cas, String plantas, String cuartos, String baños, String sector){
        ArrayList<Casa> cas1 = new ArrayList<>();
        cas.stream().filter((c) -> (String.valueOf(c.getPlantas()).equals(plantas) && String.valueOf(c.getBaños()).equals(baños) && String.valueOf(c.getCuartos()).equals(cuartos) && c.getUrb().getDireccion().equals(sector))).forEachOrdered((c) -> {
            cas1.add(c);
        });
        return cas1;
    }
    /**
     * Método que recibe la variable y su valor máximo para validar que no se exeda del rango
     * @param opcion
     * @param max 
     */
    public static void validarRango(int opcion, int max){
        while(opcion<1 || opcion>max ){
                System.out.println("Escoga una opción entre 1-"+max);
                opcion = teclado.nextInt();
            }
    }
    /**
     * Método para registrar una nueva urbanización
     * (Tiene sobrecarga)
     * @param urb 
     */
    public static void registrar(Urbanizacion urb){
        urbanizaciones.add(urb);
    }
    /**
     * Método para registrar una nueva etapa
     * (sobrecarga)
     * @param et 
     */
    public static void registrar(Etapa et){
        String otro = "S";
        while("S".equals(otro)){
            et.getServicios().add(mostarServicios());
            System.out.println("¿Desea añadir más servicios?(S/N): ");
            otro = teclado.next().toUpperCase();
        }
        Urbanizacion u = getUrbanizacion();
        u.getEtapas().add(et);
        et.setUrbanizacion(u);
        System.out.println("¡Etapa registrada!");
    }
    /**
     * Método para registrar un nuevo lote
     * (sobrecarga)
     * @param lo 
     */
    public static void registrar(Lote lo){
        Urbanizacion u = getUrbanizacion();
        lo.setUrb(u);
        u.getLotes().add(lo);
        System.out.println("¡Lote registrada!");
    }
    /**
     * Método para registrar una nueva casa
     * (sobrecarga)
     * @param casa 
     */
    public static void registrar(Casa casa){
        Urbanizacion u = getUrbanizacion();
        Etapa e = getEtapa(u);
        e.getCasas().add(casa);
        casa.setUrb(u);
        casa.setEt(e);
        System.out.println("¡Casa registrada!");
    }
    /**
     * Método para leer la lista estática -servicios- y presentar sus elementos 
     * @return 
     */
    public static Servicio mostarServicios(){
        System.out.println("*****  SERVICIOS DISPONIBLES ****");
        int i = 1;
        for (Servicio s: servicios) {
            System.out.println(i+". "+s.getNombre());
            i++;
        }
        System.out.println("Escoja un servicio: ");
        int num = teclado.nextInt();
        return servicios.get(num-1);
        
    }
    /**
     * Método que muestra todas las etapas de una urbanización y devuelve la que seleccione el usuario
     * @param u
     * @return 
     */
    public static Etapa getEtapa(Urbanizacion u){
        System.out.println("****  ETAPAS DISPONIBLES   ***");
        int i =1;
        for (Etapa e : u.getEtapas()){
            System.out.println(i+". "+e.getNombre());
            i++;
        }
        System.out.println("Elija la etapa: ");
        int op2 = teclado.nextInt();
        validarRango(op2, urbanizaciones.size());
        
        Etapa e = u.getEtapas().get(op2-1);
        return e;
    }
    /**
     * Método que muestra todas las casas de una etapa y devuelve la que seleccione el usuario
     * @param et
     * @return 
     */
    public static Casa getCasa(Etapa et){
        System.out.println("****  DISPONIBLES   ***");
        int i =1;
        for (Casa c : et.getCasas()){
            System.out.println(i+". "+c.getNombreModelo());
            i++;
        }
        System.out.println("Elija la casa/departamento: ");
        int op2 = teclado.nextInt();
        validarRango(op2, et.getCasas().size());
        
        Casa c = et.getCasas().get(op2-1);
        return c;
    }
    /**
     * Método que muestra todos los lotes de una urbanización y devuelve el que seleccione el usuario
     * @param u
     * @return 
     */
    public static Lote getLote(Urbanizacion u){
        System.out.println("****  LOTES DISPONIBLES   ***");
        int i =1;
        for (Lote l : u.getLotes()){
            System.out.println(i+". Manzana: "+l.getManzana());
            i++;
        }
        System.out.println("Elija el lote: ");
        int op2 = teclado.nextInt();
        validarRango(op2, urbanizaciones.size());
        
        Lote l = u.getLotes().get(op2-1);
        return l;
    }
    /**
     * Método que muestra todas las urbanizaciones y devuelve la que seleccione el usuario
     * @return 
     */
    public static Urbanizacion getUrbanizacion(){
        System.out.println("****  URBANIZACIONES DISPONIBLES   ***");
        int i =1;
        for (Urbanizacion u : urbanizaciones){
            System.out.println(i+". "+u.getNombre());
            i++;
        }
        
        System.out.println("Elija la urbanización: ");
        int op2 = teclado.nextInt();
        validarRango(op2, urbanizaciones.size());
        
        Urbanizacion u = urbanizaciones.get(op2-1);
        return u;
    }
    /**
     * Método encargado de guardar la lista de usuarios actualizadas en un archivo serializable
     * @param nameSerializable
     * @param urbs 
     */
    public static void serializar(String nameSerializable, ArrayList<Urbanizacion> urbs ){
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(nameSerializable))){
                oos.writeObject(urbs);
        } catch (FileNotFoundException ex) {
            System.err.println("No se encontró el archivo .ser");
        } catch (IOException ex) {
            ex.printStackTrace();
            System.err.println("Se produjo un error al serializar");
       
        }
    }
    /**
     * Método para extraer la lista de urbanizaciones serializada y llenar la lista estática -urbanizaciones-
     * @param nameSerializable
     * @return 
     */
    public static ArrayList<Urbanizacion> deserializar(String nameSerializable){
        ArrayList<Urbanizacion> urs = new ArrayList<>();
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(nameSerializable))){
                urs = (ArrayList<Urbanizacion>) ois.readObject();
                return urs ;
        } catch (FileNotFoundException ex) {
            System.err.println("No se encontró el archivo "+nameSerializable);
        } catch (ClassNotFoundException ex) {            
            System.err.println("Error en el casting del objeto a deserializar");
        } catch ( IOException ex) {
            System.err.println("No hay objeto a deserializar o la lista está vacía. Cree al menos una urbanización");
            return urs;
        }
        return urs;
    }
    /**
     * Método para llenar la lista estática de servicios a partir de la lectura del archivo servicios.txt
     */
    public static void fillServices(){
        try (Scanner sc = new Scanner(new File("servicios.txt"))) {
            while (sc.hasNextLine()) 
                servicios.add(new Servicio(sc.nextLine()));
               
        } catch (FileNotFoundException e) {
            System.err.println("Archivo no encontrado: servicios");
        }
    }
    /**
     * Método que imprime todas las etapas de una urbanizacion
     * @param u
     * @return 
     */
    public static String printEtapas(Urbanizacion u) {
        StringBuilder sb = new StringBuilder();
        u.getEtapas().forEach((e) -> {
            sb.append(e.getNombre()).append(", ");
        });
        System.out.println(sb);
        return String.valueOf(sb);
    }
    /**
     * Método para validar que sea una contraseña aceptable
     * @param contrasena 
     */
    public static void validarContrasena(String contrasena){
        Pattern p = Pattern.compile( "[0-9]" );
        Matcher m = p.matcher( contrasena );
        Pattern p2 = Pattern.compile( "[a-zA-Z]" );
        Matcher m2 = p2.matcher( contrasena );
        while(contrasena.length()<6 || !m.find() || !m2.find()){
             contrasena = teclado.next();
        }
    }
    /**
     * Método para llenar la lista estática rutas
     */
    public static void llenarRutas(){
        rutas.add("casa1.jpg");
        rutas.add("casa2.jpg");
        rutas.add("casa3.jpg");
        rutas.add("casa4.jpg");
    }
    /**
     * Método para obtener una ruta de imagen aleatoria de la lista estática rutas y seteársela a la casa cuando se la cree
     * @return 
     */
    public static String getRandomRute(){
        Random num = new Random();
        int rand = num.nextInt(rutas.size()-1);
        return rutas.get(rand);
    }
    /**
     * Método para obtener las casas que cumplen con el presupuesto del usuario sin tomar en cuenta los otros parámetros
     * @param esCasa
     * @param presupuesto
     * @return 
     */
    public static ArrayList<Casa> casasEncontradas(String esCasa, double presupuesto){
        llenarListaCasas();
        ArrayList<Casa> houses = new ArrayList<>();
        casas.forEach((c) -> {
            if(esCasa.equals("casa")){
                if(presupuesto >= c.getPrecio()){
                    houses.add(c);
                }
            }else{
                if(c instanceof Departamento){
                    if(presupuesto >= c.getPrecio()){
                        houses.add(c);
                    }
                }
            }
        });
        
        return houses;
    }
    
    /**
     * Método para calcular el presupuesto que podría dar el cliente tomando en cuenta su 40% de sueldo mensual
     * @param entrada
     * @param sueldo
     * @return 
     */
    public static double calcularPresupuesto(double entrada, double sueldo){
        double interesAnual = 0.079;
        double capital = sueldo*0.40*12*15;
        double r = Math.pow(1+((interesAnual/12)/100), -180);
        return ((capital*interesAnual/12)/(100*(1-r))*12*15)+ entrada;
    }
    
    /**
     * Método que llena la lista estática de casas que se usa en la opción 1
     */
    public static void llenarListaCasas(){
        ArrayList<Casa> c = new ArrayList<>();
        urbanizaciones.forEach((u) -> {
            u.getEtapas().forEach((e) -> {
                e.getCasas().forEach((ca) -> {
                    c.add(ca);
                });
            });
        });
        casas = c;
    }
    
}
