/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmobiliariatcy;

/**
 *
 * @author alcokond
 */
public class Departamento extends Casa {
    private int lavanderia;
    private int parqueadero;
    private int ascensor;

    public Departamento() {
        lavanderia = 0;
        parqueadero = 0;
        ascensor = 0;
    }

    public Departamento(int lavanderia, int parqueadero, int ascensor, String nombreModelo, int plantas, int cuartos, int cocina, int cuartoServicio, int baños, int metrosConstruccion, double precio) {
        super(nombreModelo, plantas, cuartos, cocina, cuartoServicio, baños, metrosConstruccion, precio);
        this.lavanderia = lavanderia;
        this.parqueadero = parqueadero;
        this.ascensor = ascensor;
    }

    public int isLavanderia() {
        return lavanderia;
    }

    public void setLavanderia(int lavanderia) {
        this.lavanderia = lavanderia;
    }

    public int isParqueadero() {
        return parqueadero;
    }

    public void setParqueadero(int parqueadero) {
        this.parqueadero = parqueadero;
    }

    public int isAscensor() {
        return ascensor;
    }

    public void setAscensor(int ascensor) {
        this.ascensor = ascensor;
    }

    @Override
    public String toString() {
        
        return "Departamento{" + "lavanderia=" + lavanderia + ", parqueadero=" + parqueadero + ", ascensor=" + ascensor + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + this.lavanderia;
        hash = 47 * hash + this.parqueadero;
        hash = 47 * hash + this.ascensor;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Departamento other = (Departamento) obj;
        if (this.lavanderia != other.lavanderia) {
            return false;
        }
        if (this.parqueadero != other.parqueadero) {
            return false;
        }
        if (this.ascensor != other.ascensor) {
            return false;
        }
        return true;
    }
    
    
    
}
