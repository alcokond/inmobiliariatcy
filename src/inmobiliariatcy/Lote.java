/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmobiliariatcy;

import java.io.Serializable;

/**
 *
 * @author alcokond
 */
public class Lote  implements Serializable{
    private Urbanizacion urb;
    private String manzana;
    private int tamaño;

    public Lote() {
        urb = null;
        manzana = "";
        tamaño = 0;
    }

    public Lote(String manzana, int tamaño) {
        this.manzana = manzana;
        this.tamaño = tamaño;
        this.urb = null;
    }

    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    public int getTamaño() {
        return tamaño;
    }

    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }

    @Override
    public String toString() {
        return "Lote{" + "manzana=" + manzana + ", tamaño=" + tamaño + '}';
    }

    public Urbanizacion getUrb() {
        return urb;
    }

    public void setUrb(Urbanizacion urb) {
        this.urb = urb;
    }
    
    
    
   
    
}
